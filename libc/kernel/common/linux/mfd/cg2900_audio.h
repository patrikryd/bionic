/****************************************************************************
 ****************************************************************************
 ***
 ***   This header was automatically generated from a Linux kernel header
 ***   of the same name, to make information necessary for userspace to
 ***   call into the kernel available to libc.  It contains only constants,
 ***   structures, and macros generated from the original header, and thus,
 ***   contains no copyrightable information.
 ***
 ****************************************************************************
 ****************************************************************************/
#ifndef _CG2900_AUDIO_H_
#define _CG2900_AUDIO_H_

#include <linux/types.h>

#define CG2900_A2DP_MAX_AVDTP_HDR_LEN 25

#define CG2900_OPCODE_SET_DAI_CONF 0x01
#define CG2900_OPCODE_GET_DAI_CONF 0x02
#define CG2900_OPCODE_CONFIGURE_ENDPOINT 0x03
#define CG2900_OPCODE_START_STREAM 0x04
#define CG2900_OPCODE_STOP_STREAM 0x05

enum cg2900_dai_dir {
 DAI_DIR_B_RX_A_TX = 0x00,
 DAI_DIR_B_TX_A_RX = 0x01
};

enum cg2900_dai_mode {
 DAI_MODE_SLAVE = 0x00,
 DAI_MODE_MASTER = 0x01
};

enum cg2900_dai_stream_ratio {
 STREAM_RATIO_FM16_VOICE16 = 0x01,
 STREAM_RATIO_FM16_VOICE8 = 0x02,
 STREAM_RATIO_FM48_VOICE16 = 0x03,
 STREAM_RATIO_FM48_VOICE8 = 0x06
};

enum cg2900_dai_fs_duration {
 SYNC_DURATION_8 = 0,
 SYNC_DURATION_16 = 1,
 SYNC_DURATION_24 = 2,
 SYNC_DURATION_32 = 3,
 SYNC_DURATION_48 = 4,
 SYNC_DURATION_50 = 5,
 SYNC_DURATION_64 = 6,
 SYNC_DURATION_75 = 7,
 SYNC_DURATION_96 = 8,
 SYNC_DURATION_125 = 9,
 SYNC_DURATION_128 = 10,
 SYNC_DURATION_150 = 11,
 SYNC_DURATION_192 = 12,
 SYNC_DURATION_250 = 13,
 SYNC_DURATION_256 = 14,
 SYNC_DURATION_300 = 15,
 SYNC_DURATION_384 = 16,
 SYNC_DURATION_500 = 17,
 SYNC_DURATION_512 = 18,
 SYNC_DURATION_600 = 19,
 SYNC_DURATION_768 = 20
};

enum cg2900_dai_bit_clk {
 BIT_CLK_128 = 0x00,
 BIT_CLK_256 = 0x01,
 BIT_CLK_512 = 0x02,
 BIT_CLK_768 = 0x03,
 BIT_CLK_1024 = 0x04,
 BIT_CLK_1411_76 = 0x05,
 BIT_CLK_1536 = 0x06,
 BIT_CLK_2000 = 0x07,
 BIT_CLK_2048 = 0x08,
 BIT_CLK_2400 = 0x09,
 BIT_CLK_2823_52 = 0x0A,
 BIT_CLK_3072 = 0x0B
};

enum cg2900_dai_sample_rate {
 SAMPLE_RATE_8 = 0,
 SAMPLE_RATE_16 = 1,
 SAMPLE_RATE_44_1 = 2,
 SAMPLE_RATE_48 = 3
};

enum cg2900_dai_port_protocol {
 PORT_PROTOCOL_PCM = 0x00,
 PORT_PROTOCOL_I2S = 0x01
};

enum cg2900_dai_channel_sel {
 CHANNEL_SELECTION_RIGHT = 0x00,
 CHANNEL_SELECTION_LEFT = 0x01,
 CHANNEL_SELECTION_BOTH = 0x02
};

struct cg2900_dai_conf_i2s_pcm {
 enum cg2900_dai_mode mode;
 enum cg2900_dai_channel_sel i2s_channel_sel;
 bool slot_0_used;
 bool slot_1_used;
 bool slot_2_used;
 bool slot_3_used;
 enum cg2900_dai_dir slot_0_dir;
 enum cg2900_dai_dir slot_1_dir;
 enum cg2900_dai_dir slot_2_dir;
 enum cg2900_dai_dir slot_3_dir;
 __u8 slot_0_start;
 __u8 slot_1_start;
 __u8 slot_2_start;
 __u8 slot_3_start;
 enum cg2900_dai_stream_ratio ratio;
 enum cg2900_dai_port_protocol protocol;
 enum cg2900_dai_fs_duration duration;
 enum cg2900_dai_bit_clk clk;
 enum cg2900_dai_sample_rate sample_rate;
};

enum cg2900_dai_half_period {
 HALF_PER_DUR_8 = 0x00,
 HALF_PER_DUR_16 = 0x01,
 HALF_PER_DUR_24 = 0x02,
 HALF_PER_DUR_25 = 0x03,
 HALF_PER_DUR_32 = 0x04,
 HALF_PER_DUR_48 = 0x05,
 HALF_PER_DUR_64 = 0x06,
 HALF_PER_DUR_75 = 0x07,
 HALF_PER_DUR_96 = 0x08,
 HALF_PER_DUR_128 = 0x09,
 HALF_PER_DUR_150 = 0x0A,
 HALF_PER_DUR_192 = 0x0B
};

enum cg2900_dai_word_width {
 WORD_WIDTH_16 = 0x00,
 WORD_WIDTH_32 = 0x01
};

struct cg2900_dai_conf_i2s {
 enum cg2900_dai_mode mode;
 enum cg2900_dai_half_period half_period;
 enum cg2900_dai_channel_sel channel_sel;
 enum cg2900_dai_sample_rate sample_rate;
 enum cg2900_dai_word_width word_width;
};

union cg2900_dai_port_conf {
 struct cg2900_dai_conf_i2s i2s;
 struct cg2900_dai_conf_i2s_pcm i2s_pcm;
};

enum cg2900_dai_ext_port_id {
 PORT_0_I2S,
 PORT_1_I2S_PCM
};

enum cg2900_audio_endpoint_id {
 ENDPOINT_PORT_0_I2S,
 ENDPOINT_PORT_1_I2S_PCM,
 ENDPOINT_SLIMBUS_VOICE,
 ENDPOINT_SLIMBUS_AUDIO,
 ENDPOINT_BT_SCO_INOUT,
 ENDPOINT_BT_A2DP_SRC,
 ENDPOINT_BT_A2DP_SNK,
 ENDPOINT_FM_RX,
 ENDPOINT_FM_TX,
 ENDPOINT_ANALOG_OUT,
 ENDPOINT_DSP_AUDIO_IN,
 ENDPOINT_DSP_AUDIO_OUT,
 ENDPOINT_DSP_VOICE_IN,
 ENDPOINT_DSP_VOICE_OUT,
 ENDPOINT_DSP_TONE_IN,
 ENDPOINT_BURST_BUFFER_IN,
 ENDPOINT_BURST_BUFFER_OUT,
 ENDPOINT_MUSIC_DECODER,
 ENDPOINT_HCI_AUDIO_IN
};

struct cg2900_dai_config {
 enum cg2900_dai_ext_port_id port;
 union cg2900_dai_port_conf conf;
};

enum cg2900_endpoint_sample_rate {
 ENDPOINT_SAMPLE_RATE_8_KHZ = SAMPLE_RATE_8,
 ENDPOINT_SAMPLE_RATE_16_KHZ = SAMPLE_RATE_16,
 ENDPOINT_SAMPLE_RATE_44_1_KHZ = SAMPLE_RATE_44_1,
 ENDPOINT_SAMPLE_RATE_48_KHZ = SAMPLE_RATE_48
};

struct cg2900_endpoint_config_a2dp_src {
 enum cg2900_endpoint_sample_rate sample_rate;
 unsigned int channel_count;
};

struct cg2900_endpoint_config_fm {
 enum cg2900_endpoint_sample_rate sample_rate;
};

struct cg2900_endpoint_config_sco_in_out {
 enum cg2900_endpoint_sample_rate sample_rate;
};

union cg2900_endpoint_config_union {
 struct cg2900_endpoint_config_sco_in_out sco;
 struct cg2900_endpoint_config_a2dp_src a2dp_src;
 struct cg2900_endpoint_config_fm fm;
};

struct cg2900_endpoint_config {
 enum cg2900_audio_endpoint_id endpoint_id;
 union cg2900_endpoint_config_union config;
};

#endif
