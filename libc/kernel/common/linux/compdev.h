/****************************************************************************
 ****************************************************************************
 ***
 ***   This header was automatically generated from a Linux kernel header
 ***   of the same name, to make information necessary for userspace to
 ***   call into the kernel available to libc.  It contains only constants,
 ***   structures, and macros generated from the original header, and thus,
 ***   contains no copyrightable information.
 ***
 ***   To edit the content of this header, modify the corresponding
 ***   source file (e.g. under external/kernel-headers/original/) then
 ***   run bionic/libc/kernel/tools/update_all.py
 ***
 ***   Any manual change here will be lost the next time this script will
 ***   be run. You've been warned!
 ***
 ****************************************************************************
 ****************************************************************************/
#ifndef _COMPDEV_H_
#define _COMPDEV_H_
#ifndef _KERNEL
#include <stdint.h>
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
#else
#include <linux/types.h>
#include <video/mcde.h>
#endif
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
#ifdef _KERNEL
#include <linux/mm_types.h>
#include <linux/bitops.h>
#else
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
#define BIT(nr) (1UL << (nr))
#endif
#define COMPDEV_DEFAULT_DEVICE_PREFIX "comp"
#define NUM_COMPDEV_BUFS 2
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
enum compdev_fmt {
 COMPDEV_FMT_RGB565,
 COMPDEV_FMT_RGB888,
 COMPDEV_FMT_RGBX8888,
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 COMPDEV_FMT_RGBA8888,
 COMPDEV_FMT_YUV422,
};
struct compdev_size {
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 uint16_t width;
 uint16_t height;
};
enum compdev_rotation {
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 COMPDEV_ROT_0 = 0,
 COMPDEV_ROT_90_CCW = 90,
 COMPDEV_ROT_180_CCW = 180,
 COMPDEV_ROT_270_CCW = 270,
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 COMPDEV_ROT_90_CW = COMPDEV_ROT_270_CCW,
 COMPDEV_ROT_180_CW = COMPDEV_ROT_180_CCW,
 COMPDEV_ROT_270_CW = COMPDEV_ROT_90_CCW,
};
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
enum compdev_ptr_type {
 COMPDEV_PTR_PHYSICAL,
 COMPDEV_PTR_HWMEM_BUF_NAME_OFFSET,
};
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
struct compdev_rect {
 __s32 x;
 __s32 y;
 __s32 width;
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 __s32 height;
};
struct compdev_buf {
 enum compdev_ptr_type type;
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 __s32 hwmem_buf_name;
 __s32 fd;
 __u32 offset;
 __u32 len;
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
};
struct compdev_img {
 enum compdev_fmt fmt;
 struct compdev_buf buf;
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 __s32 width;
 __s32 height;
 __u32 pitch;
 struct compdev_rect dst_rect;
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
};
struct compdev_post_buffers_req {
 enum compdev_rotation rotation;
 struct compdev_img img_buffers[NUM_COMPDEV_BUFS];
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
 __u8 buffer_count;
};
#define COMPDEV_GET_SIZE_IOC _IOR('D', 1, struct compdev_size)
#define COMPDEV_POST_BUFFERS_IOC _IOW('D', 2, struct compdev_post_buffers_req)
/* WARNING: DO NOT EDIT, AUTO-GENERATED CODE - SEE TOP FOR INSTRUCTIONS */
#endif
