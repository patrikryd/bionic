/****************************************************************************
 ****************************************************************************
 ***
 ***   This header was automatically generated from a Linux kernel header
 ***   of the same name, to make information necessary for userspace to
 ***   call into the kernel available to libc.  It contains only constants,
 ***   structures, and macros generated from the original header, and thus,
 ***   contains no copyrightable information.
 ***
 ****************************************************************************
 ****************************************************************************/
#ifndef STM_H
#define STM_H

#define STM_DEV_NAME "stm"

struct stm_channel {
 union {
 __u8 no_stamp8;
 __u16 no_stamp16;
 __u32 no_stamp32;
 __u64 no_stamp64;
 };
 union {
 __u8 stamp8;
 __u16 stamp16;
 __u32 stamp32;
 __u64 stamp64;
 };
};

#define STM_SW_LOSSLESS 0
#define STM_HW_LOSSY 1

enum clock_div {
 STM_CLOCK_DIV2 = 0,
 STM_CLOCK_DIV4,
 STM_CLOCK_DIV6,
 STM_CLOCK_DIV8,
 STM_CLOCK_DIV10,
 STM_CLOCK_DIV12,
 STM_CLOCK_DIV14,
 STM_CLOCK_DIV16,
};

#define STM_CONNECTION _IOW('t', 0, enum stm_connection_type)
#define STM_DISABLE _IO('t', 1)
#define STM_GET_NB_MAX_CHANNELS _IOR('t', 2, int)
#define STM_GET_NB_FREE_CHANNELS _IOR('t', 3, int)
#define STM_GET_CHANNEL_NO _IOR('t', 4, int)
#define STM_SET_CLOCK_DIV _IOW('t', 5, enum clock_div)
#define STM_GET_CTRL_REG _IOR('t', 6, int)
#define STM_ENABLE_SRC _IOWR('t', 7, int)
#define STM_GET_FREE_CHANNEL _IOW('t', 8, int)
#define STM_RELEASE_CHANNEL _IOW('t', 9, int)
#define STM_SET_MODE _IOWR('t', 10, int)
#define STM_GET_MODE _IOR('t', 11, int)

enum stm_connection_type {
 STM_DISCONNECT = 0,
 STM_DEFAULT_CONNECTION = 1,
 STM_STE_MODEM_ON_MIPI34_NONE_ON_MIPI60 = 2,
 STM_STE_APE_ON_MIPI34_NONE_ON_MIPI60 = 3,
 STM_STE_MODEM_ON_MIPI34_APE_ON_MIPI60 = 4
};

#endif
